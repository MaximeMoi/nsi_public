from IPython.display import clear_output
from ipywidgets import *
import random as rd


def exo1():
    a = rd.randint(-10,10)
    b = rd.randint(-10,10)
    c = a + b
    
    print('                  ')
    print(f'a <-- {a}')
    print(f'b <-- {b}')
    print(f'c <-- a + b')
    print('                  ')
    print(f'que vaut c ?')   
    
           
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)
                if int(aaa.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])


def exo2():
    a = rd.randint(-10,10)
    b = rd.randint(-10,10)
    c = (a + b) * (a - b)
    
    print('                  ')
    print(f'a <-- {a}')
    print(f'b <-- {b}')
    print(f'c <-- (a + b)(a - b)')
    print('                  ')
    print(f'que vaut c ?')   
    
           
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)
                if int(aaa.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])


def exo5():
    a = rd.randint(-10,10)
    b = rd.randint(-10,10)
    if a>b:
        c = a
    else:
        c = b
    
    print('                  ')
    print(f'a <-- {a}')
    print(f'b <-- {b}')
    print(f'si a > b')
    print('     afficher a')
    print('sinon')
    print('     afficher b')
    print('                  ')
    print('quel sera l\'affichage ?')   
    
           
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)
                if int(aaa.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])

def exo6():
    a = rd.randint(-10,10)
    b = rd.randint(-10,10)
    c = rd.randint(-10,10)
    
    if a>b:
        if a>c:
            d = a
        else:
            d = c
    else:
        d = b
    
    print('                  ')
    print(f'a <-- {a}')
    print(f'b <-- {b}')
    print(f'c <-- {c}')
    print(f'si a > b')
    print(f'     si a > c')
    print('         afficher a')
    print('     sinon')
    print('          afficher c')
    print('sinon')
    print('     afficher b')
    print('                  ')
    print('quel sera l\'affichage ?')   
    
           
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)
                if int(aaa.value) == d:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
                
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])


def exo7():
    a = rd.randint(2,10)
    n = rd.randint(-4,4)
    m = rd.randint(-20,20)
    print('                  ')
    print(f'm <-- {m}')
    print(f'pour i allant de 1 à {a}')
    print(f'     m <-- m + {n} ')
    print('                  ')
    print(f'que vaut m ?')
    
    for i in range(a):
        m += n   
        
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)
                if int(aaa.value) == m:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])


def exo8():
    a = rd.randint(2,10)
    m = rd.randint(-20,20)
    
    print('                  ')
    print(f'm <-- {m}')
    print(f'pour i allant de 1 à {a}')
    print(f'     m <-- m + i ')
    print('                  ')
    print(f'que vaut m ?')
    
    for i in range(a+1):
        m += i  
        
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)
                if int(aaa.value) == m:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])


def exo3():
    a = rd.randint(2,10)
    m = rd.randint(1,20)
    
    c = a + m
    
    print('                  ')
    print('fonction(a)')
    print(f'     b <-- a + {m}')
    print('     afficher b')    
    print('                  ')
    print(f'que va afficher fonction({a}) ?')
   
    
            
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)                
                if int(aaa.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])

def exo4():
    a = rd.randint(-5,5)
    b = rd.randint(-5,5)
    
    a1 = a
    b1 = b
    
    a1 = a1 + b1
    b1 = a1 - b1
    c = a1 * b1
    
    print('                  ')
    print('fonction(a,b)')
    print(f'     a <-- a + b')
    print(f'     b <-- a - b')
    print(f'     c <-- a * b')
    print('     afficher c')    
    print('                  ')
    print(f'que va afficher fonction({a},{b}) ?')
   
    
            
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)                
                if int(aaa.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])



def exo9():
    a = rd.randint(2,10)
    b = rd.randint(1,20)
    c = rd.randint(-10,10)   

    
    print('                  ')
    print('fonction(a,b)')
    print(f'     c <-- {c}')
    print(f'     pour i allant de 0 à b')
    print('           c <-- c - a')
    print('     afficher c')    
    print('                  ')
    print(f'que va afficher fonction({a},{b}) ?')

    for i in range(b+1):
        c -= a
    d = c
   
    
            
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='Réponse:',
    disabled=False
    )
    
    butt = widgets.Button(description='vérifier')
    outt = widgets.Output()
    
    def on_clicked(b):
        with outt:
            clear_output()
            try:
                int(aaa.value)                
                if int(aaa.value) == d:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt,outt])


def exo10():
    a = rd.randint(6,10)
    c = rd.randint(-10,5)   

    
    print('                  ')
    print(f'c <-- {c}')
    print(f'a <-- {a}')
    print(f'i <-- 0')
    print('tant que c < a')
    print('     c <-- c + 1')
    print('     i <-- i + 1')
    print('                  ')
    print(f'quelle sont les valeurs de i et c ?')

    i=0
    while c < a:
        c+=1
        i+=1
   
    
            
    cval=widgets.Text(
    value='',
    placeholder='',
    description='c :',
    disabled=False
    )

    ival=widgets.Text(
    value='',
    placeholder='',
    description='i :',
    disabled=False
    )
    
    cbutt = widgets.Button(description='vérifier')
    coutt = widgets.Output()

    ibutt = widgets.Button(description='vérifier')
    ioutt = widgets.Output()
    
    def on_clicked_c(b):
        with coutt:
            clear_output()
            try:
                int(cval.value)                            
                if int(cval.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')

    def on_clicked_i(b):
        with ioutt:
            clear_output()
            try:
                int(ival.value)                            
                if int(ival.value) == i:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    cval.on_submit(on_clicked_c)    
    cbutt.on_click(on_clicked_c)

    ival.on_submit(on_clicked_i)    
    ibutt.on_click(on_clicked_i)
    return widgets.VBox([ival,ibutt,ioutt,cval,cbutt,coutt])




def exo11():
    c = rd.randint(20,50)
    a = rd.randint(2,13)
    if c%a == 0:
        c += 1
        
    print('                  ')    
    print(f'a <-- {a}')
    print(f'c <-- {c}')
    print(f'i <-- 0')
    print('tant que c n\'est pas un multiple de a')
    print('     c <-- c + 1')
    print('     i <-- i + 1')
    print('                  ')
    print(f'quelle sont les valeurs de i et c ?')
    
    i = 0
    while c%a != 0:
        c += 1
        i += 1
    
    
    cval=widgets.Text(
    value='',
    placeholder='',
    description='c :',
    disabled=False
    )

    ival=widgets.Text(
    value='',
    placeholder='',
    description='i :',
    disabled=False
    )
    
    cbutt = widgets.Button(description='vérifier')
    coutt = widgets.Output()

    ibutt = widgets.Button(description='vérifier')
    ioutt = widgets.Output()
    
    def on_clicked_c(b):
        with coutt:
            clear_output()
            try:
                int(cval.value)                            
                if int(cval.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')

    def on_clicked_i(b):
        with ioutt:
            clear_output()
            try:
                int(ival.value)                            
                if int(ival.value) == i:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    cval.on_submit(on_clicked_c)    
    cbutt.on_click(on_clicked_c)

    ival.on_submit(on_clicked_i)    
    ibutt.on_click(on_clicked_i)
    return widgets.VBox([ival,ibutt,ioutt,cval,cbutt,coutt])
    
    
    





def exo12():
    a = rd.randint(2,10)
    b = rd.randint(2,10)
    n = rd.randint(-4,4)
    m = rd.randint(-20,20)

    print('                  ')
    print('c <-- 0')
    print(f'm <-- {m}')
    print(f'pour i allant de 1 à {a}')
    print(f'     pour j allant de 1 à {b}')
    print(f'        m <-- m + {n} ')
    print('        c <-- c + 1 ')

    print('                  ')
    print(f'que valent i et m ?')
    
    c=0
    for i in range(a):
        for j in range(b):
            m += n
            c += 1   
        
        
    mval=widgets.Text(
    value='',
    placeholder='',
    description='m :',
    disabled=False
    )

    ival=widgets.Text(
    value='',
    placeholder='',
    description='c :',
    disabled=False
    )
    
    mbutt = widgets.Button(description='vérifier')
    moutt = widgets.Output()

    ibutt = widgets.Button(description='vérifier')
    ioutt = widgets.Output()
    
    def on_clicked_c(b):
        with moutt:
            clear_output()
            try:
                int(mval.value)                            
                if int(mval.value) == m:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')

    def on_clicked_i(b):
        with ioutt:
            clear_output()
            try:
                int(ival.value)                            
                if int(ival.value) == c:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
            
    mval.on_submit(on_clicked_c)    
    mbutt.on_click(on_clicked_c)

    ival.on_submit(on_clicked_i)    
    ibutt.on_click(on_clicked_i)
    return widgets.VBox([ival,ibutt,ioutt,mval,mbutt,moutt])

def exo13(fonc):
    test = [ rd.randint(-100,100) for i in range(10) ]
    res = [i*5 for i in test ]
    try:
        for i in range(len(test)):
            assert res[i] == fonc(test[i])
        print('bravo!')
    except:
        print('mauvaise réponse...')
        
def exo14(fonc):
    test = [ 'tata', 'plâtre', 'déjà', 'patatra', 'aâà', 'De tous les Länder, la Bavière est le plus grand' ]
    res = [2, 1, 1, 3, 3, 4 ]
    try:
        for i in range(len(test)):
            assert res[i] == fonc(test[i])
        print('bravo!')
    except:
        print(f'mauvaise réponse..., la chaine "{test[i]}" ne fonctionne pas')
        
        
    
